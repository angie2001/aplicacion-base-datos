const axios = require("axios");

//--CUENTAS--
//Obtener todas las cuentas
const obtenerCuentas = async() =>{
    //Obtiene las tareas del backend
    const cuentas = await axios.get("http://localhost:3000/cuentas");

    console.log(cuentas.data);
}
//Obtener una cuenta
//TODO: Modificar parámetros entrada
const obtenerCuenta = async() =>{
    //Obtiene las tareas del backend
    const identificador = "619c28f89e30622c749e4ff0"
    const cuenta = await axios.get(`http://localhost:3000/cuentas/${identificador}`);

    console.log(cuenta.data);
}
//Crear una cuenta
//TODO: Modificar parámetros entrada
const crearCuenta = async() =>{
    //Crea la cuenta en backend
    const respuesta = await axios.post("http://localhost:3000/cuentas",{
        usuario: "maria",
        email: "maria@gmail.com",
        password: "$2b$10$BLA0repX3P0xqHkekyzueutAjjY7KYzUzT3NJxLkZlnjLpakV/n7G"
    });
    console.log(respuesta.data);
};



//--CATEGORÍAS--
//Obtener todas las categorías
const obtenerCategorias = async() =>{
    const categorias = await axios.get(`http://localhost:3000/categorias_cuentas`);

    console.log(categorias.data);
}
//Obtener una categoría
//TODO: Modificar parámetros entrada
const obtenerCategoria = async() =>{
    //Obtiene las tareas del backend
    const identificador = "619c66dea79d8451837fa9e4"
    const categoria = await axios.get(`http://localhost:3000/categorias_cuentas/${identificador}`);

    console.log(categoria.data);
}
//Crear una categoria
//TODO: Modificar parámetros entrada
const crearCategoria = async() =>{
    //Crea la cuenta en backend
    const respuesta = await axios.post("http://localhost:3000/categorias_cuentas",{
        email: "lina@gmail.com",
        categoria: "Plato fuerte"
    });
    console.log(respuesta.data);
};
//Editar una categoría
//TODO: Modificar parámetros entrada
const editarCategoria = async() =>{
    //Crea la cuenta en backend
    const identificador = "619c30d7a0e52ced33258149"
    const respuesta = await axios.patch(`http://localhost:3000/categorias_cuentas/${identificador}`,{
        categoria: "Almuerzo"
    });
    console.log(respuesta.data);
};
//Elimina una categoria
//TODO: Modificar parámetros entrada
const eliminarCategoria = async() =>{
    //Elimina la tarea del backend
    const identificador = "619c66dea79d8451837fa9e4"
    const categoria = await axios.delete(`http://localhost:3000/categorias_cuentas/${identificador}`);

    console.log(categoria.data);
}


//--GASTOS--
//Obtener todos los gastos
const obtenerGastos = async() =>{
    const gasto = await axios.get(`http://localhost:3000/gastos_cuentas`);

    console.log(gasto.data);
}
//Obtener un gasto
//TODO: Modificar parámetros entrada
const obtenerGasto = async() =>{
    //Obtiene las tareas del backend
    const identificador = "619c3da80e682d4997132e48"
    const gasto = await axios.get(`http://localhost:3000/gastos_cuentas/${identificador}`);

    console.log(gasto.data);
}
//Crear una categoria
//TODO: Modificar parámetros entrada
const crearGasto = async() =>{
    //Crea la cuenta en backend
    const respuesta = await axios.post("http://localhost:3000/gastos_cuentas",{
        email: "lina@gmail.com",
        categoria: "Plato fuerte",
        gasto: 32000,
        descripcion: "Pasta",
        fecha: "2021/03/15"
    });
    console.log(respuesta.data);
};
//Editar un gasto
//TODO: Modificar parámetros entrada
const editarGasto = async() =>{
    //Crea la cuenta en backend
    const identificador = "619d4e95b27084b9bcacd37c"
    const respuesta = await axios.patch(`http://localhost:3000/gastos_cuentas/${identificador}`,{
        categoria: "Comida",
        gasto: 20200,
        descripcion: "Carne",
        fecha: "2021/02/29"
    });
    console.log(respuesta.data);
};
//Eliminar un gasto
//TODO: Modificar parámetros entrada
const eliminarGasto = async() =>{
    //Obtiene las tareas del backend
    const identificador = "619c3da80e682d4997132e48"
    const gasto = await axios.delete(`http://localhost:3000/gastos_cuentas/${identificador}`);

    console.log(gasto.data);
}


module.exports ={
    obtenerCuentas,
    obtenerCuenta,
    crearCuenta,
    obtenerCategorias,
    obtenerCategoria,
    crearCategoria,
    obtenerGastos,
    obtenerGasto,
    crearGasto,
    eliminarCategoria,
    eliminarGasto,
    editarCategoria,
    editarGasto,
}