//Importa ExpressJS
const express = require("express");
const Categoria = require('../models/categoria')

//crea el router
const router = express.Router();


//Todas las rutas asociadas con /categorias_cuentas
//"/cuentas"
router.get('/',async(req,res)=>{
    try{
        const datosCategorias = await Categoria.find();

        return res.status(200).send(datosCategorias);
    }catch(error){
        res.status(500).send({mensaje:"Error en el servidor"});
    }
});

//OBTIENE SOLO UUNA CATEGORÍA SEGÚN EL ID
router.get('/:id',async(req,res)=>{
    try{
        const id = req.params.id;

        const categoriaObtenida = await Categoria.findById(id);

        if(categoriaObtenida === null){
            return res.status(404).send({mensaje: "No se encontró la categoría"}); 
        }else{
            return res.status(200).send(categoriaObtenida);
        }
    }catch(error){
        res.status(500).send({mensaje:"Error en el servidor"});
    }

});

//CREAR UNA NUEVA CATEGORÍA
router.post('/',async(req,res) =>{
    //Obtiene el body
    const {email,categoria} = req.body;

    try{
        const categoriaNueva = new Categoria({
            email,
            categoria
        });

        let resultado = await categoriaNueva.save();

        res.status(201).send(resultado);
    }catch(error){
        res.status(500).send({mensaje:"Error en el servidor"}); 
    }

})

//EDITAR UNA CATEGORÍA SEGÚN EL ID
router.patch('/:id',async(req,res)=>{
    //Obtiene el id de los parámetros
    const id = req.params.id;
    //Obtiene el body de la petición
    const body = req.body;
    try{
        const resultado = await Categoria.findOneAndUpdate(
            {_id:id},
            body,
            {
                new: true
            }
        );
        //retorna el objeto actualizado al cliente
        return res.status(201).send(resultado);

    }catch(error){
        return res.status(500).send({mensaje:"Error en el servidor"}); 
    }


})


//ELIMINAR UNA CATEGORÍA SEGÚN EL ID
router.delete('/:id',async(req,res) =>{
    //Obtener el id
    const id = req.params.id;
    try{
        //Elmimina la tarea de la BD
        const resultado = await Categoria.deleteOne({
            _id:id
        })
        //Retorna la respuesta al cliente
        return res.status(200).send(resultado);
    }catch(error){
        res.status(500).send({mensaje:"Error en el servidor"}); 
    }

})


module.exports = router;