//Importa los paquetes
const express = require("express");
const Gasto = require('../models/gasto')

//crea el router
const router = express.Router();


//Todas las rutas asociadas con /cuentas
//"/cuentas"
router.get('/',async(req,res)=>{
    try{
        const datosGastos = await Gasto.find();

        return res.status(200).send(datosGastos);
    }catch(error){
        res.status(500).send({mensaje:"Error en el servidor"});
    }
});

//OBTIENE SOLO UN GASTO SEGÚN EL ID
router.get('/:id',async(req,res)=>{
    
    try{
        const id = req.params.id;
        const gastoObtenido = await Gasto.findById(id);

        if(gastoObtenido === null){
            return res.status(404).send({mensaje: "No se encontró el gasto"});
        }else{
            return res.status(200).send(gastoObtenido);
        }
        
    }catch(error){
        res.status(500).send({mensaje:"Error en el servidor"});
    }
});

//CREAR UN NUEVO GASTO
router.post('/',async(req,res) =>{
    const {email,categoria,gasto,descripcion,fecha} = req.body;

    try{
        const gastoNuevo = new Gasto({
            email,
            categoria,
            gasto,
            descripcion,
            fecha
        });
        let resultado = await gastoNuevo.save();

        res.status(201).send(resultado);
    }catch(error){
        res.status(500).send({mensaje:"Error en el servidor"});
    }

})

//EDITAR UN GASTO SEGÚN EL ID
router.patch('/:id',async(req,res) =>{
    const id = req.params.id;
    const body = req.body;

    try{
        const resultado = await Gasto.findByIdAndUpdate(
            {_id:id},
            body,
            {new: true}
        );

        return res.status(201).send(resultado);

    }catch(error){
        res.status(500).send({mensaje:"Error en el servidor"}); 
    }

})

//ELIMIAR UN GASTO SEGÚN EL ID
router.delete('/:id',async(req,res) =>{
    const id = req.params.id;
    try{
        const resultado = await Gasto.deleteOne({_id:id})

        return res.status(200).send(resultado);
    }catch(error){
        res.status(500).send({mensaje:"Error en el servidor"}); 
    }
})

module.exports = router;