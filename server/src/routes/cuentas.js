//Importa los paquetes
const express = require("express");
const Cuenta = require('../models/cuenta')

//crea el router
const router = express.Router();


//Todas las rutas asociadas con /cuentas
router.get('/',async(req,res)=>{  
    try{
        //Oobtiene la lista de tareas de la BD
        const datosCuentas = await Cuenta.find();

        //Retorna las tareas al cliente
        return res.status(200).send(datosCuentas);
    }catch(error){
        res.status(500).send({mensaje:"Error en el servidor"});
    }
});

//Un elemento del arreglo espacifico por el id 
router.get('/:id',async(req,res)=>{
    try{
        //obtiene el id del parámetro
        const id = req.params.id;
        
        //obtiene la tarea con el id recibido
        const cuentaObtenida = await Cuenta.findById(id);

        //Retorna la cuenta al cliente
        if(cuentaObtenida === null){
            return res.status(404).send({mensaje: "No se encontró la cuenta"});
        }else{
            return res.status(200).send(cuentaObtenida);
        }

    }catch(error){
        res.status(500).send({mensaje:"Error en el servidor"});
    }
});

//CREAR UNA NUEVA CUENTA    
router.post('/',async(req,res) =>{
    //Obtiene el body
    const {usuario,email,password} = req.body;
    
    try{
        //Crea el objeto Cuenta
        const cuentaNueva = new Cuenta({
            usuario,
            email,
            password
        });
        //Guarda el objeto en la base de datos
        let resultado = await cuentaNueva.save();

        //Retorna el objeto creado al cliente
        res.status(201).send(resultado);

    }catch(error){
        res.status(500).send({mensaje:"Error en el servidor"});
    }
})

module.exports = router;

