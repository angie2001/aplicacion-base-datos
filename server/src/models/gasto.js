const mongoose = require('mongoose');

const GastoSchema = mongoose.Schema(
    {
        email:{
            type: String,
            require: true,
        },
        categoria:{
            type: String,
            default:"general",
        },
        gasto:{
            type: Number,
            require: true
        },
        descripcion:{
            type: String,
            require: true,
        },
        fecha:{
            type: Date,
            default: Date.now,
        },
    }
);

module.exports = mongoose.model("Gasto", GastoSchema)