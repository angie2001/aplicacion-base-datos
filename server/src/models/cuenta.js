const mongoose = require('mongoose');

const CuentaSchema = mongoose.Schema(
    {
        usuario: {
            type: String,
            require: true,
        },
        email:{
            type: String,
            require: true,
        },
        password:{
            type: String,
            require: true,
        },
    }
);

//Exportar esquema
module.exports = mongoose.model("Cuenta", CuentaSchema)