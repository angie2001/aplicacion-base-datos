const mongoose = require('mongoose');

const CategoriaSchema = mongoose.Schema(
    {
        email:{
            type: String,
            require: true,
        },
        categoria:{
            type: String,
            require: true,
        },
    }
);

module.exports = mongoose.model("Categoria", CategoriaSchema)