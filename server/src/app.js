//Importa paquetes externos
const express = require("express");
require("dotenv").config();
const mongoose = require("mongoose");


//Establece de las rutas
const cuentasRoute = require('./routes/cuentas');
const gastosCuentasRoute = require('./routes/gastos_cuentas');
const categoriasCuentasRoute = require('./routes/categorias_cuentas');



//Crea la aplicación
const app = express();

//Permite leer el body de las peticiones
app.use(express.json())

//Middleware de todos los datos registrados
app.use("/cuentas", cuentasRoute);
app.use("/categorias_cuentas",categoriasCuentasRoute);
app.use("/gastos_cuentas",gastosCuentasRoute);

//RUTAS
app.get("/",(req, res) => {
    res.send("Bienvenido al API");
});


//CONEXIÓN A LA BASE DE DATOS
mongoose.connect(process.env.CONEXION_MONGODB,{
    useUnifiedTopology: true,
    useNewUrlParser: true
    },
    ()=>{
    console.log('Conectado a la base de datos ...');
    }
);

//Ejecuta el servidor  
app.listen(3000);